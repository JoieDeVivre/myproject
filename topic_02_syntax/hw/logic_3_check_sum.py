"""
Функция check_sum.

Принимает 3 числа.
Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
"""


def check_sum(a, b, c):
    return a + b == c or a + c == b or b + c == a


if __name__ == '__main__':
    result = check_sum(2, 5, 7)
    print(result)
