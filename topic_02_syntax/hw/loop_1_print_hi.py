"""
Функция print_hi.

Принимает число n.
Выведите на экран n раз фразу "Hi, friend!"
Если число <= 0, тогда вывести пустую строку.
Пример: n=3, тогда в результате "Hi, friend!Hi, friend!Hi, friend!"
"""


def print_hi(number):
    text = '' if number <= 0 else f'{number * "Hi, friend!"}'
    print(text)


if __name__ == '__main__':
    print_hi(2)