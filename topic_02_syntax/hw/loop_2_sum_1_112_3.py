"""
Функция sum_1_112_3.

Вернуть сумму 1+4+7+10+...109+112.
"""


def sum_1_112_3():
    a = range(1, 113, 3)
    return sum(a)


if __name__ == '__main__':
    result = sum_1_112_3()
    print(result)
