"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число меньше или равно 0, то вернуть "Must be > 0!".
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
"""


def count_odd_num(x):
    odd = 0

    if type(x) != int:
        return "Must be int!"

    elif 0 >= x:
        return "Must be > 0!"

    else:
        while x > 0:
            if x % 2 != 0:
                odd += 1
            x = x // 10
        return odd


if __name__ == '__main__':
    result = count_odd_num(65439)
    print(result)
