"""
Функция print_nth_symbols.

Принимает строку и натуральное число n (целое число > 0).
Вывести символы с индексом n, n*2, n*3 и так далее.
Пример: string='123456789qwertyuiop', n = 2 => result='3579wryip'

Если число меньше или равно 0, то вывести строку 'Must be > 0!'.
Если тип n не int, то вывести строку 'Must be int!'.

Если n больше длины строки, то вывести пустую строку.
"""


def print_nth_symbols(string, n):
    if type(n) != int:
        print('Must be int!')
    elif n <= 0:
        print('Must be > 0!')
    else:
        i = 1
        while n * i < len(string):
            print(string[n * i], end='')
            i += 1
        print()


if __name__ == '__main__':
    print_nth_symbols('thefourthstring', 6.3)  # Must be int!
    print_nth_symbols('thethirdstring', -3)  # Must be > 0!
    print_nth_symbols('thefirststring', 16)  # there's an empty string
    print_nth_symbols('thesecondstring', 0)  # Must be > 0!
    print_nth_symbols('thefifthstring', 1)
    print_nth_symbols('123456789qwertyuiop', 5)
