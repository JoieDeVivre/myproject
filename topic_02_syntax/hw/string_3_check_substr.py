"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""


def check_substr(string_1, string_2):
    if len(string_1) == len(string_2):
        return False
    elif len(string_2) > len(string_1):
        return string_1 in string_2
    elif len(string_1) > len(string_2):
        return string_2 in string_1
    else:
        return False


if __name__ == '__main__':
    print(check_substr('thefourthstring', 'four'))
    print(check_substr('thefourthstring', 'one'))
    print(check_substr('', 'four'))
    print(check_substr('four', 'four'))
